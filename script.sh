#!/usr/bin/env bash

echo "Hello, we will push the code source to Gitlab."
git config --global user.name "Aymane REIDA"
git config --global user.email "aymanerei53@gmail.com"
echo -n "Write your commit message : "
read -r message
git status
git add .
git commit -m "$message"
git push -u origin master
clear
echo "Code source pushed to Gitlab"